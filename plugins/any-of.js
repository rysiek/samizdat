/* ========================================================================= *\
|* === Any-of: running multiple plugins simultaneously                   === *|
\* ========================================================================= */

/**
 * this plugin does not implement any push method
 */

// no polluting of the global namespace please
(function () {
    
    /*
     * plugin config settings
     */
    
    // sane defaults
    let defaultConfig = {
        // name of this plugin
        // should not be changed
        name: "any-of",
        // list of plugins to run simultaneously
        plugins: {
            "alt-fetch": {},
            "gun-ipfs": {}
        }
    }
    
    // merge the defaults with settings from SamizdatConfig
    let config = {...defaultConfig, ...self.SamizdatConfig.plugins[defaultConfig.name]}
    
    /**
     * getting content using regular HTTP(S) fetch()
     */
    let fetchContent = (url) => {
        self.log(config.name, `using: [${Object.keys(config.plugins).join(', ')}]!`)
        return Promise.any(
            SamizdatPlugins
                .filter(p=>Object.keys(config.plugins).includes(p.name))
                .map(p=>p.fetch(url))
        )
    }

    // and add ourselves to it
    // with some additional metadata
    self.SamizdatPlugins.push({
        name: config.name,
        description: `Running simultaneously: [${Object.keys(config.plugins).join(', ')}]`,
        version: 'COMMIT_UNKNOWN',
        fetch: fetchContent,
        uses: config.plugins
    })

// done with not poluting the global namespace
})()
