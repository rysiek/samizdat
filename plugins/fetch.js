/* ========================================================================= *\
|* === Regular HTTP(S) fetch() plugin                                    === *|
\* ========================================================================= */

/**
 * this plugin does not implement any push method
 */

// no polluting of the global namespace please
(function () {
    
    /*
     * plugin config settings
     */
    
    // sane defaults
    let defaultConfig = {
        // name of this plugin
        // should not be changed
        name: "fetch"
    }
    
    // merge the defaults with settings from SamizdatConfig
    let config = {...defaultConfig, ...self.SamizdatConfig.plugins[defaultConfig.name]}
    /**
     * getting content using regular HTTP(S) fetch()
     */
    let fetchContent = (url) => {
        self.log(config.name, `regular fetch: ${url}`)
        return fetch(url, {cache: "reload"})
            .then((response) => {
                // 4xx? 5xx? that's a paddlin'
                if (response.status >= 400) {
                    // throw an Error to fall back to Samizdat:
                    throw new Error('HTTP Error: ' + response.status + ' ' + response.statusText);
                }
                // all good, it seems
                self.log(config.name, `fetched successfully: ${response.url}`);
                
                // we need to create a new Response object
                // with all the headers added explicitly,
                // since response.headers is immutable
                var init = {
                    status:     response.status,
                    statusText: response.statusText,
                    headers: {}
                };
                response.headers.forEach(function(val, header){
                    init.headers[header] = val;
                });
                
                // add the X-Samizdat-* headers to the mix
                init.headers['X-Samizdat-Method'] = config.name
                init.headers['X-Samizdat-ETag'] = response.headers.get('ETag')
                
                // return the new response, using the Blob from the original one
                return response
                        .blob()
                        .then((blob) => {
                            return new Response(
                                blob,
                                init
                            )
                        })
            })
    }

    // and add ourselves to it
    // with some additional metadata
    self.SamizdatPlugins.push({
        name: config.name,
        description: 'Just a regular HTTP(S) fetch()',
        version: 'COMMIT_UNKNOWN',
        fetch: fetchContent
    })

// done with not poluting the global namespace
})()
