#!/usr/bin/env node

const yargs = require("yargs");
const split = require("split");
const isIPFS = require("is-ipfs");
const fetch = require("node-fetch");

// we will deal with that later, in setup_gun()
// this is because we don't want the annoying Gun welcome message
// if we're not actually using Gun
var gun = false
var peers = [/*'https://gunjs.herokuapp.com/gun', 'http://cdn.test.occrp.org:8080/gun'*/'https://samizdat.is/gun']

// Gun instance vars
// actual setup in setup_gun()
var gunConnection = false;
var gunConnectionTwo = false

var confirmations = 0;
var updatedPaths = [];

// are we in debug mode
var debug = false;


/* +-----------------------------------------------------------------------+ *\
|* | general utility function                                              | *|
\* +-----------------------------------------------------------------------+ */

/*
 * load and set-up Gun
 * 
 * Gun pollutes the console output with an annoying message
 * so we don't want to load it until we have to
 */
let setup_gun = () => {
    gun = require("gun");
    gun.SEA.throw = true;

    // set the Gun instances up
    debuglog('setup_gun(): gunConnection')
    gunConnection = gun({
        peers: peers,
        localStorage: false,
        radisk: false,
        axe: false
    });
    // we need two to get reliable confirmation of success
    debuglog('setup_gun(): gunConnectionTwo')
    gunConnectionTwo = gun({
        peers: peers,
        localStorage: false,
        radisk: false,
        axe: false
    });

}

/*
 * accepts any number of arguments
 * and works similar to console.log(a, b, c)
 */
let debuglog = (...msg) => {
    if (debug) {
        console.log('DEBUG ::', msg.join(' '))
    }
}


/* +-----------------------------------------------------------------------+ *\
|* | low-level functions                                                   | *|
\* +-----------------------------------------------------------------------+ */

/**
 * auth a Gun user,
 * using either a username+password combination, or a pubkey
 * 
 * username - name/alias of the user
 * password - password to use for authenticating as the `username` user
 * pubkey   - alternatively, a pubkey can be used for auth (but functionality will be limited)
 */
let gun_auth_user = (username=false, password=false, pubkey=false) => {
    debuglog(`gun_auth_user()`)
    setup_gun()
    debuglog(`gun_auth_user(): post-setup_gun`)
    return new Promise(
        (resolve, reject) => {
            debuglog(`gun_auth_user(): in Promise()`)
            if (username && password) {
                debuglog(`gun_auth_user(): we have username and password`)
                gunAPI = gunConnection.user()
                debuglog(`gun_auth_user(): gunAPI: ${gunAPI}`)
                gunAPI.auth(username, password, function(userReference){
                    debuglog(`gun_auth_user(): auth callback`)
                    if (userReference.err) {
                        reject(new Error(userReference.err))
                    } else {
                        console.log('Update user authenticated using password.');
                        gunAPITwo = gunConnectionTwo.user(gunAPI.is.pub);
                        debuglog("user's pubkey:", gunAPI.is.pub)
                        console.log('Verification user authenticated from a pubkey.');
                        resolve(gunAPI)
                    }
                })
            } else if (pubkey) {
                gunAPI = gunConnection.user(pubkey);
                console.log('User authenticated from a pubkey.');
                resolve(gunAPI)
            } else {
                console.log('ERROR: Neither user and password, nor a public key of a user were provided,');
                console.log('ERROR: so there is no Gun account to work with!');
                reject(new Error("Not able to authenticate the user with Gun."))
            }
        }
    )
}


/**
 * getting a user's public key from Gun
 * 
 * username - the name of the user to get the pubkey of
 * gconn    - optional gun connection reference in case we don't want to use
 *            the default gunConnection
 */
let gun_get_user_pubkey = (username, gconn=false) => {
    debuglog('pre-setup_gun')
    setup_gun()
    debuglog('post-setup_gun')
    gconn = gconn ? gconn : gunConnection
    debuglog(`pre-promise; username: ${username}`)
    return new Promise(
        (resolve, reject) => {
            if (typeof username === 'string') {
                // c.f. https://gun.eco/docs/User#getting-a-user-via-alias
                gconn
                    .get(`~@${username}`)
                    .once((data, key) => {
                        if (data === undefined) {
                            debuglog(`User doesn't seem to exist: ${key}`)
                            reject(new Error(`User doesn't seem to exist: ${key}`))
                        } else {
                            // the user exists! return the pubkey
                            // yes, we're making a very strict assumption that in Gun the public key
                            // always comes after the '_' object 
                            resolve(Object.keys(data)[1].substr(1))
                        }
                    })
            } else {
                console.log('ERROR: cannot check if user exists: no username provided.')
                reject(new Error('Unable to verify the user exists: no username provided'))
            }
        }
    )
} 


/**
 * checking if a Gun user exists
 * using either the alias/username, or a pubkey
 * 
 * optionally, check if the Gun user has the specified pubkey
 * (if both username and pubkey are provided)
 * 
 * username - username/alias of the user
 * pubkey   - of the user
 */
let gun_check_user_exists = (username=false, pubkey=false) => {
    setup_gun()
    return new Promise(
        (resolve, reject) => {
            if (username) {
                // we have the username;
                // if can get the pubkey from Gun, user exists
                gun_get_user_pubkey(username)
                    .then((userpub) => {
                        // yay! user exists!
                        // do we have a pubkey to test the one we got from Gun against?
                        if (pubkey) {
                            if (userpub === pubkey) {
                                resolve(true)
                            } else {
                                debuglog(`Provided pubkey does not match the one in Gun for user: ${username}.`)
                                resolve(false)
                            }
                        // no pubkey to test against, but we know the user exists, yay!
                        } else {
                            resolve(true)
                        }
                    })
                    .catch((err)=>{
                        debuglog(`User ${username} does not exist.`)
                        resolve(false)
                    })
            } else if (pubkey) {
                // we only have the pubkey
                // c.f. https://gun.eco/docs/User#getting-a-user-via-gun-user
                gunConnection
                    .user(pubkey)
                    .once((data, key) => {
                        if (data === undefined) {
                            debuglog(`User with pubkey ${pubkey} does not exist.`)
                            resolve(false)
                        } else {
                            debuglog(`User with pubkey ${pubkey} exists: ${data.alias}`)
                            resolve(true)
                        }
                    })
            } else {
                debuglog('Cannot check if a user exists: neither username nor pubkey specified.')
                reject(new Error('Cannot check if a user exists: neither username nor pubkey specified.'))
            }
        }
    )
}


// TODO: This is dumb ─ need a strategy.
let parse_gun_key = (key) => {
    // at the very least they cannot be empty!
    debuglog('key: ' + key)
    if (!key) {
        console.error('ERROR: Samizdat domain and path must not be empty!')
        process.exit(2)
    }
    return key;
}


// TODO: is this needed?
let parse_gun_value = (value) => {
    return value;
}


/**
 * getting the data to be updated (that is, pushed to Gun)
 * either from the arguments, or from stdin
 * 
 * domain  - the domain being processed
 * path    - either the path being processed,
 *           or `true` to signify that paths and addreses need to be retrieved from stdin 
 * address - IPFS address related to the path (if path !== true) 
 */
let gun_get_update_data = (domain, path, address=false) => {
    return new Promise(
        (resolve, reject) => {
            
            var update = {};
            
            // path will be `true` if it was passed at the CLI as '-'
            if (path !== true) {
                // regular CLI operation, easy peasy
                if (isIPFS.path(address)) {
                    update[path] = address;
                } else if (isIPFS.cid(address)) {
                    update[path] = '/ipfs/' + address;
                } else {
                    reject(new Error(`This is not a valid IPFS address/CID: ${address}`))
                }
                resolve([domain, update])
            
            } else {
                // we need to get stuff from stdin
                process.stdin
                    .pipe(split())
                    .on('data', (line) => {
                        debuglog('got a line:', line)
                        if (line) {
                            line = line.split(' ', 2)
                            if (line[0] && line[1]) {
                                if (isIPFS.path(line[1])) {
                                    update[line[0]] = line[1]
                                } else if (isIPFS.cid(line[1])) {
                                    update[line[0]] = '/ipfs/' + line[1]
                                }
                            }
                        }
                    })
                    .on('close', () => {
                        debuglog('got a close')
                        resolve([domain, update])
                    });
            }
        }
    );
}


/* +-----------------------------------------------------------------------+ *\
|* | high-level functions, handling cli actions directly                   | *|
\* +-----------------------------------------------------------------------+ */


let gun_view = (argv) => {
    debuglog('gun_view()')
    gun_auth_user(argv.user, argv.password, argv.pubkey)
        .then(() => {
            debuglog(`gun_view(): user authed, getting the data: ${argv.domain}:${argv.path}`)
            return gunAPI
            .get(argv.domain)
            .get(argv.path)
            .once(function(addr){
                    console.log('[' + argv.domain + ']' + argv.path + " :: " + addr);
                },
                {wait: 5000})
        })
        .then(process.exit)
        .catch(error => {
            console.log("Failed to view Gun data: ", error);
            process.exit(1)
        })
}


let gun_update = (argv) => {
    setup_gun()
    gun_auth_user(argv.user, argv.password, argv.pubkey)
        .then(() => {
            return gun_get_update_data(
                // clean the data being passed to the low-level handler
                parse_gun_key(argv.domain),
                parse_gun_key(argv.path),
                argv.address ? parse_gun_key(argv.address) : false
            );
        })
        .then(domain_and_update => {
            var domain = domain_and_update[0]
            var update = domain_and_update[1]
            // debug
            debuglog('updating ' + domain + ' with: ')
            debuglog(JSON.stringify(update, undefined, 2))
            
            if (Object.entries(update).length === 0 && update.constructor === Object) {
                throw new Error('Nothing to update!')
            }
            
            gunAPI.get(domain).put(update, function(ack) {
                debuglog('confirmation?', ack)
                if (ack.err) {
                    //throw new Error("Confirmation error: ", ack.err)
                    debuglog('Confirmation error (ignoring)', ack.err)
                // ack.ok is *optional*: https://gun.eco/docs/API#callback-ack-
                // so, anything that does not contain ack.err is a success
                } else {
                    confirmations++;
                    if (ack.ok) {
                        console.log("Got confirmation #" + confirmations + ' (message: ' + ack.ok + ')')
                    } else {
                        console.log("Got confirmation #" + confirmations + ' (no message)')
                    }
                    if (confirmations >= argv.minconf) {
                        // Done!
                        console.log("Got " + argv.minconf + " confirmations.");
                        process.exit();
                    }
                }
            }, {acks: argv.minconf})
            
            // chain things
            return domain_and_update;
        })
        .then(domain_and_update => {
            /*
             * regular confirmations don't seem to work
             * 
             * so instead we're creating a second user, using a separate Gun instance
             * and using that to .get() the data that we've .put() just a minute ago
             * 
             * we then subscribe to the .on() events and once we notice the correct
             * addresseswe consider our job done and quit.
             */
            var domain = domain_and_update[0]
            updatedPaths = Object.keys(domain_and_update[1])
            for (path in domain_and_update[1]) {
                
                debuglog('watching path for updates:', path)
                gunAPITwo.get(domain).get(path).on(function(updaddr, updpath){
                    debuglog('+--', updpath)
                    debuglog('    updated  :', domain_and_update[1][updpath])
                    debuglog('    received :', updaddr)
                    if (domain_and_update[1][updpath] == updaddr) {
                        // update worked!
                        gunAPITwo.get(domain).get(updpath).off()
                        console.log('+-- update confirmed for:', updpath, '[' + updaddr + ']')
                        var pathIndex = updatedPaths.indexOf(updpath)
                        if (pathIndex > -1) {
                            updatedPaths.splice(pathIndex, 1)
                        }
                        if (updatedPaths.length === 0) {
                            console.log('All updates confirmed successful!')
                            process.exit();
                        }
                    }
                // ToDo: what happens when we hit the timeout here?
                }, {wait: 5000})
            }
            return domain_and_update;
        })
        .then(ret => {
            // this is basically a nasty hack to work around the fact
            // that confirmations just don't work as they should
            // we want the process running for a few seconds so that
            // data propagates
            debuglog('starting the confirmations-do-not-work timeout...')
            setTimeout(function(){
                debuglog('confirmations-do-not-work timeout fired, exiting cleanly...')
                process.exit(0);
            }, 10000)
        })
        .catch(error => {
            console.log('ERROR:', error)
            process.exit(1)
        });
}


/**
 * getting a Gun user's public key
 * 
 * argv.user - username we're interested in
 */
let gun_user_pubkey = (argv) => {
    debuglog('gun_user_pubkey()')
    gun_get_user_pubkey(argv.user)
        .then((pubkey) => {
            console.log(pubkey)
            process.exit(0)
        })
        .catch((err)=>{
            console.log(err)
            process.exit(1)
        })
}


/**
 * creating a Gun user account
 * 
 * NOTICE: this requires Node 10.x or higher!
 * 
 * argv.user     - username/alias to use
 * argv.password - password to register the user with
 */
let gun_user_create = (argv) => {

    // make sure we're running a version of Node that can do this
    if (process.version.split('.')[0].substr(1) < 10) {
        console.log(`ERROR: creating Gun users requires Node v10 or later! Running ${process.version}.`)
        process.exit(1)
    }

    setup_gun()
    gun_check_user_exists(argv.user)
        .then((user_exists)=>{
            // that's a paddlin'!
            if (user_exists) {
                console.log(`ERROR: user ${argv.user} seems to already exist.`)
                process.exit(1)
            }
            var gunUser = gunConnection.user()
            console.log(`Creating a Gun user '${argv.user}' with password '${argv.password}'...`)
            // c.f. https://gun.eco/docs/User#user-create
            gunUser
                .create(argv.user, argv.password, (ack)=>{
                    if (ack.err) {
                        console.log(`ERROR: Unable to create user '${argv.user}':`, ack.err)
                        process.exit(1)
                    } else {
                        if (ack.ok === 0) {
                            console.log(`User '${argv.user}' successfully created.`)
                            console.log(`Pubkey: ${ack.pub}`)
                            console.log("\nWaiting for the new user to propagate through the Gun network...")
                            return ack.pub
                        } else {
                            console.log('ERROR: Unknown error, this should not happen!')
                            process.exit(1)
                        }
                    }
                })
        })
        // confirmations do not work, so we're using a separate Gun instance
        // to pull the user data from the network and check if the pubkey matches
        .then((pub)=>{
            debuglog('+-- verifying the new user propagated into the Gun network')
            gunConnectionTwo
                .get(`~@${argv.user}`)
                .on((data, key) => {
                    if (data !== undefined) {
                        var datapub = Object.keys(data)[1].substr(1)
                        debuglog(`    +-- received data, pubkey: ${datapub}`)
                        if (pub == datapub) {
                            // update worked! disable further processing
                            gunConnectionTwo
                                .get(`~@${argv.user}`)
                                .off()
                            console.log(`User data sucessfully propagated through the Gun network.`)
                            process.exit(0);
                        } else {
                            debuglog(`        +-- pubkey does not match: ${pub}`)
                        }
                    }
                // ToDo: what happens when we hit the timeout here?
                }, {wait: 5000})
        })
        .then(ret => {
            // this is basically a nasty hack to work around the fact
            // that confirmations just don't work as they should
            // we want the process running for a few seconds so that
            // data propagates
            debuglog('starting the confirmations-do-not-work timeout...')
            setTimeout(function(){
                debuglog('confirmations-do-not-work timeout fired, exiting cleanly...')
                process.exit(0);
            }, 10000)
        })
        .catch((err)=>{
            console.log(`ERROR: Unable to create user '${argv.user}': ${err}`)
            process.exit(1)
        })
}


/**
 * deleting a Gun user account
 * 
 * argv.user     - username/alias to use
 * argv.password - password to register the user with
 */
let gun_user_delete = (argv) => {
    setup_gun()
    gun_check_user_exists(argv.user)
        .then((user_exists)=>{
            // that's a paddlin'!
            if (!user_exists) {
                console.log(`ERROR: user ${argv.user} does not seem to exist.`)
                process.exit(1)
            }
            var gunUser = gunConnection.user()
            // c.f. https://gun.eco/docs/User#user-delete
            gunUser
                .delete(argv.user, argv.password, (ack)=>{
                    if (ack.err) {
                        console.log(`ERROR: Unable to delete user '${argv.user}'':`, ack.err)
                        process.exit(1)
                    } else {
                        if (ack.ok === 0) {
                            console.log(`User '${argv.user}' successfully deleted.`)
                            console.log("\nWaiting for user deletion to propagate through the Gun network...")
                            return true;
                        } else {
                            console.log('ERROR: Unknown error, this should not happen!')
                            process.exit(1)
                        }
                    }
                })
        })
        // let's have a bit of a delay here
        .then(() => new Promise((resolve) => setTimeout(resolve, 5000)))
        // confirmations do not work, so we're using a separate Gun instance
        // to pull the user data from the network and check if the pubkey matches
        .then(()=>{
            debuglog('+-- verifying user deletion propagated into the Gun network')
            gunConnectionTwo
                .get(`~@${argv.user}`)
                .on((data, key) => {
                    if (typeof data === 'undefined') {
                        gunConnectionTwo
                            .get(`~@${argv.user}`)
                            .off()
                        console.log(`User deletion sucessfully propagated through the Gun network.`)
                        process.exit(0);
                    } else {
                        var datapub = Object.keys(data)[1].substr(1)
                        debuglog(`    +-- received data, user still exists`)
                        debuglog(`        pubkey: ${datapub}`)
                    }
                // ToDo: what happens when we hit the timeout here?
                }, {wait: 5000})
        })
        .then(ret => {
            // this is basically a nasty hack to work around the fact
            // that confirmations just don't work as they should
            // we want the process running for a few seconds so that
            // data propagates
            debuglog('starting the confirmations-do-not-work timeout...')
            setTimeout(function(){
                debuglog('confirmations-do-not-work timeout fired, exiting cleanly...')
                process.exit(0);
            }, 10000)
        })
        .catch((err)=>{
            console.log(`ERROR: Unable to create user '${argv.user}': ${err}`)
            process.exit(1)
        })
}


let gun_setup_gun = (argv) => {
    debuglog('gun_setup_gun()')
    setup_gun()
    debuglog(`gun_setup_gun(): Gun set-up, starting timeout (${argv.timeout}ms)`)
    setTimeout(()=>{
        debuglog(`gun_setup_gun(): we're done`)
        process.exit(0)
    }, argv.timeout)
}


/**
 * getting IPFS CIDs to be verified
 * either from the arguments, or from stdin
 * 
 * address - either the IPFS paths (or CIDs) being processed,
 *           or `true` to signify that paths and addreses need to be retrieved from stdin 
 */
let ipfs_get_cids_to_verify = (CIDs) => {
    debuglog(`ipfs_get_cids_to_verify(): got CIDs: ${CIDs} (${typeof CIDs} :: ${Array.isArray(CIDs)})`)
    return new Promise(
        (resolve, reject) => {
            // CIDs will be `true` if it was passed at the CLI as '-'
            if (CIDs !== true) {
                
                // we definitely want an array
                if (!Array.isArray(CIDs)) {
                    CIDs = [CIDs]
                }
                
                // reality check
                CIDs.forEach((cid, index) => {
                    // we want CIDs, not IPFS paths
                    if (isIPFS.path(cid)) {
                        // remove '/ipfs/' from the beginning and we're golden
                        CIDs[index] = cid.slice(6);
                    // if it's not an IPFS path, it needs to be a valid CID
                    } else if (!isIPFS.cid(cid)) {
                        reject(new Error(`This is not a valid IPFS address/CID: ${cid}`))
                    }
                })
                // did we get *anything*?
                if (CIDs.length > 0) {
                    // we're done here; just make sure we're not repeating things
                    resolve([...new Set(CIDs)])
                } else {
                    reject(new Error(`No valid IPFS address/CIDs found`))
                }
            
            // okay, so, getting the data from stdin then
            } else {
                CIDs = []
                // we need to get stuff from stdin
                process.stdin
                    .pipe(split())
                    .on('data', (line) => {
                        debuglog('got a line:', line)
                        // ignoring empty lines
                        if (line) {
                            // let's be nice and trim any whitespace
                            line = line.trim()
                            // ignoring empty lines
                            if (line != '') {
                                if (isIPFS.path(line)) {
                                    CIDs.push(line.slice(6))
                                } else if (isIPFS.cid(line)) {
                                    CIDs.push(line)
                                } else {
                                    reject(new Error(`This is not a valid IPFS address/CID: ${line}`))
                                }
                            }
                        }
                    })
                    .on('close', () => {
                        debuglog('got a close')
                        // did we get *anything*?
                        if (CIDs.length > 0) {
                            // we're done here; just make sure we're not repeating things
                            resolve([...new Set(CIDs)])
                        } else {
                            reject(new Error(`No valid IPFS address/CIDs found`))
                        }
                    });
            }
        }
    );
}


let ipfs_verify = (argv) => {
    debuglog('ipfs_verify()')
    
    // this will become useful later
    //const gatewaysJSONUrl = "https://ipfs.github.io/public-gateway-checker/gateways.json";
    // important:
    // we cannot use gateways that use hash directly in the (sub)domain:
    // https://github.com/node-fetch/node-fetch/issues/260
    var gateways = [
        'https://ninetailed.ninja/ipfs/:hash',       // Russia
        'https://10.via0.com/ipfs/:hash',            // USA
        'https://ipfs.sloppyta.co/ipfs/:hash',       // UK
        'https://gateway.temporal.cloud/ipfs/:hash', // Germany
        'https://ipfs.best-practice.se/ipfs/:hash'   // Sweden
    ]
    
    // pick 3 gateways, at random
    var useGateways = new Array()
    while (useGateways.length < 3) {
        // put in the address while we're at it
        useGateways.push(
            gateways
                .splice(Math.floor(Math.random() * gateways.length), 1)[0]
        )
    }
    debuglog(`using IPFS gateways:\nDEBUG :: +-- ${useGateways.join('\nDEBUG :: +-- ')}`)
    
    // get the data (from CLI or stdin), normalize it,
    // and do some basic reality-checks
    ipfs_get_cids_to_verify(argv.address)
        .then(CIDs=>{
            debuglog('CIDs:\nDEBUG :: +--', CIDs.join('\nDEBUG :: +-- '))
    
            // we want success rates *per CID*
            var success = {}
            // but we only need to keep track of pending fetch() requests in bulk
            var done = 0
            
            let doneCallback = () => {
                var status = 0
                clearTimeout(done_timeout)
                debuglog(`doneCallback()`)
                CIDs.forEach(cid=>{
                    if (success[cid]) {
                        console.log(`${cid} OK (${success[cid]}/3 gateways)`)
                    } else {
                        status = 2
                        console.log(`${cid} FAIL`)
                    }
                })
                process.exit(status)
            }
            
            // start the timer
            var done_timeout = setTimeout(doneCallback, 20000)
            
            // start our promises
            CIDs
                .forEach((cid)=>{
                    useGateways.map(gw => {
                        success[cid] = 0
                        url = gw.replace(':hash', cid)
                        return fetch(url)
                                .then(result => {
                                    done++
                                    if (result.ok) success[cid]++
                                    debuglog(`fulfilled: ${cid} :: ${result.ok} :: ${result.status} :: ${result.url}`)
                                    if (done == (CIDs.length * 3)) doneCallback()
                                })
                                .catch(err => {
                                    done++
                                    debuglog(` rejected: ${cid} :: ${err}`)
                                    if (done == (CIDs.length * 3)) doneCallback()
                                })
                    })
                })
        
        })
        .catch(err=>{
            console.log(`${err}`)
            process.exit(1)
        })
}


const options = yargs
    .scriptName("samizdat-cli")
    .showHelpOnFail(true)
    .demandCommand(1, '')
    .usage("Samizdat!\n\nUsage:\n  $0 [options] <command> [positionals]")
    .option("u", { alias: "user", describe: "Gun User", type: "string", demandOption: false })
    .option("p", { alias: "password", describe: "Gun User's password", type: "string", demandOption: false })
    .option("P", { alias: "peer", describe: "Upstream Gun peer (can be used more than once)", type: "string", demandOption: false })
    .option("k", { alias: "pubkey", describe: "Gun User's public key", type: "string", demandOption: false })
    .option("m", { alias: "minconf", describe: "Minimum confirmations for updates", type: "int", default: 1, demandOption: false })
    .option("d", { alias: "debug", describe: "Enable debug output", type: "boolean", default: false, demandOption: false })
    .middleware((argv)=>{
        
        // make sure debug is sanely available globally
        debug = argv.debug
        
        // same for peers, of which we can have multiple
        if (typeof argv.peer != "undefined") {
            
            // array? cool!
            if (Array.isArray(argv.peer)) {
                peers = argv.peer
                debuglog(`Gun peers set from --peer: ${argv.peer}`)
            // string? cool!
            } else if (typeof argv.peer == "string") {
                peers = [argv.peer]
                debuglog(`Gun peer set from --peer: ${argv.peer}`)
            // this should never happen
            } else {
                console.log(`ERROR: Unsupported value for --peer: ${argv.peer} (type: ${typeof argv.peer})`)
                process.exit(3)
            }
            
            // let's double check if all peers at least start with "https://"
            peers.forEach(peer=>{
                if (peer.length == 0) {
                    console.log('ERROR: --peer value cannot be empty')
                    process.exit(3)
                }
                if (peer.substr(0, 4)!='http') {
                    console.log(`ERROR: --peer value is not a valid URL: ${peer}`)
                    process.exit(3)
                }
            })
            
        } else {
            debuglog(`using default Gun peers: ${argv.peer}`)
        }
    })
    .command(
        "gun-view <domain> <path>",
        "View a Samizdat domain:path Gun entry.",
        (yargs) => {
            yargs
                .positional('domain', {'describe': "Domain to work with."})
                .positional('path', {'describe': "Path to view."})
        },
        gun_view)
    .command(
        "gun-update <domain> <path|-> [address]",
        "Update a Gun node.",
        (yargs) => {
            yargs
                .positional('domain', {'describe': "Domain to update a path in."})
                .positional('path', {'describe': "Path to update, or '-' to get paths and IPFS addresses from stdin."})
                .positional('address', {'describe': "IPFS address to update it with."})
        },
        gun_update)
    .command(
        "gun-user-pubkey <user>",
        "Get a Gun user's public key.",
        (yargs) => {
            yargs
                .positional('user', {'describe': "Gun user whose pubkey is needed."})
        },
        gun_user_pubkey)
    .command(
        "gun-user-create <user> <password>",
        "Register a Gun user; returns the user's pubkey upon success.",
        (yargs) => {
            yargs
                .positional('user', {'describe': "Username we want to register."})
                .positional('password', {'describe': "Password to register the user with."})
        },
        gun_user_create)
    .command(
        "gun-user-delete <user> <password>",
        "Delete a Gun user.",
        (yargs) => {
            yargs
                .positional('user', {'describe': "Username of the user we want to delete."})
                .positional('password', {'describe': "Password of the user."})
        },
        gun_user_delete)
    .command(
        "gun-setup <timeout>",
        "Setup gun and wait <timeout>ms. This is only useful for debugging.",
        (yargs) => {
            yargs
                .positional('timeout', {'describe': "Time (ms) to wait for."})
        },
        gun_setup_gun)
    .command(
        "ipfs-verify <address|->",
        "Verify content propagated to other IPFS nodes",
        (yargs) => {
            yargs
                .positional('address', {'describe': "IPFS address(es)/CID(s) to verify, or '-' to get them from stdin."})
        },
        ipfs_verify)
    .help()
    .argv;
