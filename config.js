/*
 * Samizdat config
 * 
 * This is the config for Samizdat as deployed on the https://samizdat.is/ site
 * 
 * When deploying Samizdat on your website you will need to create your own config,
 * using this one as a template.
 */

// plugins config
self.SamizdatConfig.plugins = {
                        'fetch':{},
                        'cache':{},
                        'any-of': {
                            plugins: {
                                'alt-fetch':{
                                    // configuring the alternate endpoints plugin to use IPNS gateways
                                    // 
                                    // NOTICE: we cannot use CIDv0 with gateways that use hash directly in the (sub)domain:
                                    //         https://github.com/node-fetch/node-fetch/issues/260
                                    //         we *can* use CIDv1 with such gateways, and that's suggested:
                                    //         https://docs.ipfs.io/how-to/address-ipfs-on-web/#path-gateway
                                    //         https://cid.ipfs.io/#QmYGVgGGfD5N4Xcc78CcMJ99dKcH6K6myhd4Uenv5yJwiJ
                                    endpoints: [
                                        'https://bafzbeietqrkxepxn7b2eotyqt7rmfspcwkttwdvs2bfin5chmeqav25qlm.ipns.dweb.link/',     // USA
                                        'https://ipfs.kxv.io/ipns/QmYGVgGGfD5N4Xcc78CcMJ99dKcH6K6myhd4Uenv5yJwiJ/',                // Hong Kong
                                        'https://jorropo.net/ipns/QmYGVgGGfD5N4Xcc78CcMJ99dKcH6K6myhd4Uenv5yJwiJ/',                // France
                                        'https://gateway.pinata.cloud/ipns/QmYGVgGGfD5N4Xcc78CcMJ99dKcH6K6myhd4Uenv5yJwiJ/',       // Germany
                                        'https://bafzbeietqrkxepxn7b2eotyqt7rmfspcwkttwdvs2bfin5chmeqav25qlm.ipns.bluelight.link/' // Singapore
                                    ]
                                },
                                'gun-ipfs': {
                                    gunPubkey: 'WUK5ylwqqgUorceQRa84qfbBFhk7eNRDUoPbGK05SyE.-yohFhTzWPpDT-UDMuKGgemOUrw_cMMYWpy6plILqrg'
                                }
                            }
                        }
                    }

// we need to explicitly list components we want to see debug messages from
self.SamizdatConfig.loggedComponents = ['service-worker', 'fetch', 'cache', 'any-of', 'alt-fetch', 'gun-ipfs']
